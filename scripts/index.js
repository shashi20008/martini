var SerialPort = require('serialport').SerialPort;
var http = require('http');
var keypress = require('keypress');
var events = new require('events');
var app = new events.EventEmitter();

var PORT = '/dev/cu.usbmodem1451';
var BAUD_RATE = 38400;
var DEVICE_ID = "MARTINI_DEF";
var MARTINI_SERVER_URL = "martini-trackon.rhcloud.com";
var MARTINI_SERVER_PORT = 3000;

var ARDUINO_CONN = undefined;
var BUFFER = undefined;

function handleKeyPress (chunk, key) {
	if(key && key.ctrl && (key.name === 'q' || key.name === 'c')) {
		if(ARDUINO_CONN && ARDUINO_CONN.isOpen()) {
			return ARDUINO_CONN.close(function(error) {
				process.exit(0);
			});
		}
		process.exit(0);
	}
}

function setupSerialPort() {
	ARDUINO_CONN = new SerialPort(PORT, {baudrate: BAUD_RATE});
	ARDUINO_CONN.on('open', function (){
		console.log('serial port open');
	});

	ARDUINO_CONN.on('error', function (err) {
		console.log('An Serial error occured: ' + err);
	});

	ARDUINO_CONN.on('data', function (data) {
		BUFFER = (BUFFER ? BUFFER : '') + data;
		while (BUFFER.indexOf('\n')  != -1) {
			var packet = BUFFER.substring(0, BUFFER.indexOf('\n'));
			BUFFER = BUFFER.substring(BUFFER.indexOf('\n')+ 1, BUFFER.length);
			var parts = packet.trim().split(':');
			if(parts.length < 2 || parseInt(parts[0]) === NaN || parseInt(parts[1]) === NaN) {
				console.log('invalid packet... skipping.');
				continue;
			}
			var postBody = {
				deviceId: DEVICE_ID,
				timestamp: parts[0],
				capacity: parts[1]
			};
			restClient(MARTINI_SERVER_URL, MARTINI_SERVER_PORT, 'POST', postBody);
		}
	});

	ARDUINO_CONN.on('close', function () {
		console.log('serial port closed.');
		setupSerialPort();
	});
}

function init() {
	var stdin = process.stdin;
	keypress(stdin);
	stdin.setRawMode(true);
	stdin.resume();
	stdin.on('keypress', handleKeyPress);
	
	console.log('setting up serial port');
	setupSerialPort();
}

function restClient(host, port, method, body) {
	body.user = "martiniweb";
	var jsonBody = JSON.stringify(body);
	var options = {
		host: host,
		//port: port,
		path: '/data',
		method: method,
		headers: {
			'Content-Type' : 'application/json',
			'Content-Length': jsonBody.length
		}
	};
	var req = http.request(options, function(res) {
		console.log('request sent');
		res.on('data', function(chunk) {console.log(chunk.toString());});
	});
	req.write(jsonBody);
	req.end();
}

init();