#include <SPI.h>
#include "RF24.h"

struct RadioPacket {
  unsigned long capacity;
  unsigned long timestamp;
};

RadioPacket _packet;

RF24 radio(7,8);

void setup() {
  Serial.begin(38400);
  
  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio.openWritingPipe((const uint8_t*)"2Mart");
  radio.openReadingPipe(1,(const uint8_t*)"1Mart");
  radio.startListening();
}

void loop() {
  if( radio.available()){
    radio.read(&_packet, sizeof(RadioPacket));        
    Serial.print(_packet.timestamp);
    Serial.print("000");
    Serial.print(":");
    Serial.println(_packet.capacity);  
 }
} // Loop

