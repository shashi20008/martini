#define CONFIG_UNIXTIME
#include <ds3231.h>
#include <Wire.h>
#include <SPI.h>
#include "RF24.h"

struct RadioPacket{
  unsigned long capacity;
  unsigned long timestamp;
};
RadioPacket packet;

RF24 radio(7,8);

byte statusLed    = 13;

byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;

// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;

volatile byte pulseCount;  

float flowRate;
unsigned int flowMilliLitres;

unsigned long oldTime, curTime;
struct ts _timestamp;

void setup()
{
  // Setup sensor
  Serial.begin(38400);
  
  Serial.println("Settings started");
  // Setup DS3231
  Wire.begin();
  DS3231_init(DS3231_INTCN);
  
  pinMode(statusLed, OUTPUT);
  digitalWrite(statusLed, HIGH);
  
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);

  // Initialize everything
  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  oldTime           = 0;
  curTime           = 0;
  memset(&_timestamp, 0, sizeof(struct ts));
  
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);

  radio.begin();

  radio.setPALevel(RF24_PA_LOW);

  radio.openWritingPipe((const uint8_t*)"1Mart");
  radio.openReadingPipe(1,(const uint8_t*)"2Mart");
  Serial.println("Settings done");
}

void loop()
{
   DS3231_get(&_timestamp);
   if((millis() - oldTime) > 1000)    
  {
    Serial.println("in If");
     curTime = _timestamp.unixtime;
    if(curTime == 0) {
      curTime = get_unixtime(_timestamp);
    }
    Serial.print("pulseCount: ");
    Serial.println(pulseCount);
    Serial.print("curTime: ");
    Serial.print("sec ");
    Serial.print(_timestamp.sec);
    Serial.print(", ");
    Serial.print("sec ");
    Serial.print(_timestamp.sec);
    Serial.print(", ");
    Serial.print("min ");
    Serial.print(_timestamp.min);
    Serial.print(", ");
    Serial.print("hour ");
    Serial.print(_timestamp.hour);
    Serial.print(", ");
    Serial.println(curTime);
    
    detachInterrupt(sensorInterrupt);
    
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    
    flowMilliLitres = (flowRate / 60) * 1000;

    Serial.println(flowMilliLitres);
    if (flowMilliLitres != 0) {
      packet.capacity = flowMilliLitres;
      packet.timestamp = curTime;

      Serial.println(F("Now sending"));
      Serial.print(flowMilliLitres);
      Serial.println("mL");

       if (!radio.write( &packet, sizeof(RadioPacket))){
         Serial.println(F("failed"));
       } else {
         Serial.println(F("sent"));
       }
    }
    pulseCount = 0;
    oldTime = millis();
   
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING); 
  }
}

void pulseCounter()
{
  pulseCount++;
}

